<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        User::factory(5)->create();
        User::create([
            'name' => 'Super Admin',
            'email' => 'superadmin@gmail.com',
            'email_verified_at'  => now(),
            'role' => 'superadmin',
            "phone" => "08323243232",
            "bio" => "saya super admin",
            'password' => Hash::make('password'),
        ]);

        User::create([
            'name' => 'Irfan Prasetyo',
            'email' => 'irfanprasetyo91@gmail.com',
            'email_verified_at'  => now(),
            'role' => 'admin',
            "phone" => "08323243232",
            "bio" => "saya super admin",
            'password' => Hash::make('password'),
        ]);
    }
}
